# Comparative fecundity and longevity of 17 strains of Trichogramma cacoeciae

This project contains all the different files used to obtain the results and figures presented in Cointe et al. 2023.

"Comparative fecundity and longevity of 17 strains of Trichogramma cacoeciae".

An R script called "Longevity_Fecundity_Script" with all the codes from the analyses to the different graphs.

And two csv files. One entitled "Forfecundity.csv" to obtain the data and graphs relative to fecundity results.

A second file entitled "Longevity.csv" to obtain the data and graphs relative to longevity results.


